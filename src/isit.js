
/**
  A collection of static methods that differentiate the type a target.

  To curry the target into a property field with results for each method,
  use `Isit.for(target)`.  i.e. `Isit.for(target)`
*/
class Isit {

  /** @return `true` if the value is an Array instance */
  static isArray(any) {
    return (any instanceof Array)
  }

  /** @return `true` if the value is any type of function*/
  static isFunction(any) {
    return (any instanceof Function)
  }

  /** @return `true` if the value is a String */
  static isString(any) {
    return (typeof any == 'string')
  }

  /** @return `true` if the value is a Number.*/
  static isNumber(any) {
    return (typeof any == 'number')
  }

  /** @return `true` if the value is a function, but not a constructor */
  static isPlainFunction(any) {
    return Isit.isFunction(any)
        && !Isit.isClass(any)
  }

  /** true for Object and any function with name.length > 1 */
  static isClass(any) {
    let func = any

    // Doesn't follow rules, but gets a free pass
    if (func === Object)
      return true

    return (
      /* Classes are functions */
      Isit.isFunction(func)
      /* Have prototypes */
      && func.prototype
      /* Prototypes are target for new keyword. */
      && Isit.isFunction(func.prototype.constructor)
      /* And they're not anonymous */
      && func.name.length > 0
    )
  }

  /*
  Technically, everything is an object, so this filters out derivative, non-value
  container types.  Returns true for instances (including literals) and prototypes.
  */
  static isObject(any) {
    return !Isit.isFunction(any)
        && !Isit.isArray(any)
        && !Isit.isString(any)
        && !Isit.isNumber(any)
  }


  /**
    @return `true` if the taret is an object but not a prototype.
  */
  static isPlainObject(any) {
    return Isit.isObject(any)
        && !Isit.isPrototype(any)
  }

  /*
    @return `true` if the target is it's own prototype, or `Object.prototype`, else `false`.
  */
  static isPrototype(any)
  {
    const target = any
    if (!Isit.isObject(target))
      return false
    if (target == Object.prototype)
      return true

    return target.constructor
        && target.constructor.prototype
        && target.constructor.prototype === target
  }

  /**
    @return {TypeTester} for `target`.
  */
  static for(target) {
    return new IsitTarget(target)
  }
}

/**
  Wraps all of the static tests from Isit as declarative values.

  It is a curry for the shared Isit.isXXX(target), which results in a
  property map for each test.
*/
class IsitTarget {
  constructor(target)
  {
    this.target = target
  }

  get class()     {return Isit.isClass(this.target)}
  get function()  {return Isit.isFunction(this.target)}
  get prototype() {return Isit.isPrototype(this.target)}
  get object()    {return Isit.isObject(this.target)}

  get array()     {return Isit.isArray(this.target)}
  get string()    {return Isit.isString(this.target)}
  get number()    {return Isit.isNumber(this.target)}

  get proto()     {return this.prototype}
  get func()      {return this.function}
}

module.exports = Isit
