const src = '../src'

const isit = require(`${src}/isit`)

const assert = require('assert')

class A {
  constructor()
  {
    this.cval = 1
  }
  get gval() {return 1}
  static sval() {return 1}
}

const a = new A()

function func () {}
const anon = ()=>{}
const obj = {}
const arr = []
const string = 'test-string'

describe('TestType', function() {
  describe('Language Fundamentals', function () {

    it('detects Numbers', function() {
      assert(!isit.isNumber(string) )
      assert(!isit.isNumber(arr) )
      assert(!isit.isNumber(obj) )
      assert(!isit.isNumber(A) )
      assert(!isit.isNumber(Array) )
      assert(!isit.isNumber(Array.prototype))
      assert(!isit.isNumber(anon) )
      assert( isit.isNumber(1))
    })

    it('detects Strings', function() {
      assert( isit.isString(string) )
      assert(!isit.isString(arr) )
      assert(!isit.isString(obj) )
      assert(!isit.isString(A) )
      assert(!isit.isString(Array) )
      assert(!isit.isString(Array.prototype))
      assert(!isit.isString() )
      assert(!isit.isString(1))
    })

    it('detects Function types', function() {
      assert(!isit.isFunction(arr) )
      assert(!isit.isFunction(obj) )
      assert( isit.isFunction(A) )
      assert( isit.isFunction(Array) )
      assert(!isit.isFunction(Array.prototype))
      assert( isit.isFunction(anon) )
    })

    it('differentiates classes and anonymous functions', function() {
      assert(!isit.isPlainFunction(A))
      assert( isit.isClass(A))

      assert( isit.isClass(Object))
      assert(!isit.isPlainFunction(Object))

      assert( isit.isClass(Array))
      assert(!isit.isPlainFunction(Array))

      assert( isit.isClass(func))
      assert(!isit.isPlainFunction(func))

      assert(!isit.isClass(anon))
      assert( isit.isPlainFunction(anon))
    })

    it('detects Array instances', function() {
      assert( isit.isArray(arr) )
      assert(!isit.isArray(obj) ) //not object
      assert(!isit.isArray(A) ) //not class
      assert(!isit.isArray(Array) )
      assert(!isit.isArray(Array.prototype))
      assert(!isit.isArray() )
      assert(!isit.isArray(string) )
      assert(!isit.isArray(1))
    })

    it('differentiates objects from Arrays, Strings, and Functions', function() {
      assert(!isit.isObject(string) )
      assert(!isit.isObject(arr) )
      assert( isit.isObject(obj) )
      assert(!isit.isObject(A) )
      assert( isit.isObject(Object.prototype))
      assert(!isit.isObject(Array) )
      assert( isit.isObject(Array.prototype))
      assert(!isit.isObject(anon) )
      assert(!isit.isObject(1))
    })

    it('differentiates between plain objects and prototypes', function() {
      assert( isit.isPrototype(A.prototype))
      assert(!isit.isPrototype(a))

      assert(!isit.isPlainObject(A.prototype))
      assert( isit.isPlainObject(a))

      assert( isit.isPlainObject(obj))
      assert(!isit.isPlainObject(arr))
    })

    it('differentiates between class, and prototypes', function() {
      assert( isit.isPrototype(A.prototype) )
      assert( isit.isPrototype(Object.prototype) )
      assert( isit.isPrototype(Array.prototype) )
      assert( isit.isPrototype(func.prototype) )

      assert(!isit.isPrototype(A) )
      assert(!isit.isPrototype(Object) )
      assert(!isit.isPrototype(Array) )
      assert(!isit.isPrototype(func) )
    })
  })
})
