# @ jamesarlow/isit
A type tester for fundamental javascript.

## Overview

Isit is a simple utility class which provides static methods to test essential type information about javascript values.

### Test Fundamental Types
* isArray
* isFunction
* isString
* isNumeric

### Differentiate Hierarchy from Instance
* `isObject` : i.e. none-of-the-above
* `isPrototype` : i.e. `A.prototype`, but not `A` or `new A`
* `isPlainObject` : i.e. `isObject` and `!isPrototype`
* `isClass` : i.e. A function with a prototype (not anonymous)
* `isPlainFunction` : i.e. A function without a prototype (anonymous)

## Installation

`npm install @jamesarlow/isit`
